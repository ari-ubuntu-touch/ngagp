
// Remove gap at top
document.getElementById("jsid-app").childNodes[1].classList.add("nheader")
document.getElementById("jsid-app").style.marginTop = "-50px"


var throttleTimer;
const throttle = (callback, time) => {
  if (throttleTimer) return;
 
  throttleTimer = true;
 
  setTimeout(() => {
    callback();
    throttleTimer = false;
  }, time);
};
const handleInfiniteScroll = () => {
  throttle(() => {
    // Add controls to videos
    for (let item of document.getElementsByTagName("video")) {item.setAttribute("controls", true);}



    // More content blocking...
    // Remove promoted posts
    var elements = document.getElementsByClassName("post-cell");
    for (var i=0;i<elements.length;i++) {
        var elem = elements[i];
        if (elem.innerHTML.indexOf("Promoted") != -1) {
            elem.innerHTML = "";
        }
    }

  }, 3000);
};
window.addEventListener("scroll", handleInfiniteScroll);




// Actual Ad Blocking!
// Nope it didnt work.
// const regex = new RegExp('doubleclick|facebook|analytics|quantserve|stakingsmile|amazon|googleapis');
//
// window.fetch = new Proxy(window.fetch, {
//     apply(fetch, that, args) {
//
//         console.log("I've got a fetch... Shall I allow it?");
//         console.log(args[0])
//         if (!regex.test(args[0])) {
//           // Forward function call to the original fetch
//           const result = fetch.apply(that, args);
//
//           //Do whatever you want with the resulting Promise
//           result.then((response) => {
//               console.log("fetch completed!", args, response);
//             console.log(response.body)
//           });
//
//           return result;
//         } else {
//           console.log("Its an ad lets block it!")
//         }
//     }
// });





var css = `
/************* ADS *************/

/* Remove top add */
#top-adhesion {display: none;}

/* Remove Inline ads */
.salt-section {display: none;}

// /* Remove Sponsored Posts*/
// .post-cell  {display: none;}
// .photo {display: block;}
// .video {display: block;}
// .gif {display: block;}

/* There's never a good reason for iframes */
iframe {display:none !important;}

/* Don't 'campaign' me */
.campaign-banner {display: none !important;}

/* Remove overlay ad */
.overlay-ad-modal {display: none;}

/* Remove top gap */
.nheader {margin-top: -50px;}

/* Fix user navigation */
/*.navbar__container {position: fixed;top: 0px;}*/
.fluid-2{margin-top: 90px;}
.navbar{position: fixed;top: 0;width: 100%;}
.navbar.no-shadow {box-shadow: none!important;position: initial;}
.post-tab-bar__container.sticky {
  top: 38px !important;
}

/********* Open In App *********/

/* Hide Open in App Overlay */
body {overflow: auto !important;}
.scroll-lock {overflow: auto !important;}

/* Remove 'open in app' footer */
.footer {display: none;}
.open-in-app {display: none !important;}

/* Remove 'Use App'*/
.use-app {display: none !important;}

/* Hide 'Get the app' banner above comments */
.inline-banner {display: none;}

/*********** Tweaks ***********/

/* Add Video Controls */
.controls[data-state="hidden"] {
  display: none;
}


/* Hide scrollbar */
body::-webkit-scrollbar {display: none;}
.featured-tag {overflow: hidden !important;}

/* Shrink Titles */
h3 {font-size: 1.2em;}

/* Shrink Tags */
.post-tag {
    font-size: 10px;
    padding: 3px 16px 0 !important;
}
.post-tag a {
    line-height: normal !important;
    padding: 2px !important;
    margin-bottom: 0px !important;
    height: auto !important;
}
.post-tags {
    height: 30px;
    padding: 1px !important;
}

/* coMuniTTy GuIdlIneS */
.comments-guideline {
  display: none !important;
}

/* Pure Dark mode */
body.theme-dark .post-cell,
body.theme-dark div.post-tag,
body.theme-dark .navbar,
body.theme-dark .tabbar,
body.theme-dark article .post-content,
body.theme-dark .comment-entry,
body.theme-dark .comment-embed,
body.theme-dark .featured-tag,
body.theme-dark .menu-list,
body.theme-dark .post-tags,
body.theme-dark .tab-bar,
body.theme-dark .post-tab-bar__container,
body.theme-dark .comment {
    background-color: #000 !important;
}

/* Add Branding :) */
.ninegag::after {
    content: "+";
    margin-left: 52px;
    font-size: 25px;
    font-weight: bold;
    line-height: 20px;
}
`


// Add css to site
var style=document.createElement('style');
style.type='text/css';
if(style.styleSheet){
    style.styleSheet.cssText=css;
}else{
    style.appendChild(document.createTextNode(css));
}
document.getElementsByTagName('head')[0].appendChild(style);
