/*
 * Copyright (C) 2022  Red
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ninegagp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.Window 2.2
import Morph.Web 0.1
import QtWebEngine 1.7
import Qt.labs.settings 1.0
import QtSystemInfo 5.5


MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'ngagp.red'

    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent

        header:  WebView {
	        id: webview
	        anchors{ fill: parent}

	        enableSelectOverride: true


	        settings.fullScreenSupportEnabled: true
	        property var currentWebview: webview
	        settings.pluginsEnabled: true

	        onFullScreenRequested: function(request) {
	          nav.visible = !nav.visible
	          request.accept();
	        }



	        profile:  WebEngineProfile{
	          id: webContext
	          persistentCookiesPolicy: WebEngineProfile.ForcePersistentCookies
	          property alias dataPath: webContext.persistentStoragePath

	          dataPath: dataLocation


	          
	          httpUserAgent: "Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.5195.58 Mobile Safari/537.36"
	        }

	        anchors{
	          fill:parent
	        }

	        url: "https://9gag.com/"
	        userScripts: [
	          WebEngineScript {
		    injectionPoint: WebEngineScript.DocumentReady
		    worldId: WebEngineScript.MainWorld
		    name: "QWebChannel"
		    sourceUrl: "ubuntutheme.js"
	          }
	        ]

	          
	      }

    }
}
