# 9GAG+ 

## Unofficial 9GAG webapp for Ubuntu Touch

9GAG+ contains tweaks for making the Ubuntu Touch 9GAG experience better.

## Features

 - No Ads
 - No 'Open In App' annoyances
 - Hidden Scrollbar
 - Added video controls
 - Smaller Post Titles
 - Smaller Tags
 - Pure Dark Mode

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/ngagp.red)

Disclaimer: This project is in no way related to 9GAG
